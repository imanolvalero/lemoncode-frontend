# Master Front End XI - Módulo 5 - Testing - Laboratorio testing


__IMPORTANTE__: Esta práctica las tenéis que subir a vuestro repositorio de Github (o si lo preferís Gitlab o Bitbucket público).

### Introducción

Partiendo del ejemplo [05-testing/01-react/05-real-project/00-boilerplate](https://github.com/Lemoncode/master-frontend-lemoncode/tree/master/05-testing/01-react/05-real-project/00-boilerplate) del repositorio, recordad que es una copia del proyecto real de origin-front-admin

#### Pasos a seguir

* Copiar los ficheros de la carpeta 00-boilerplate del repositorio anterior, en local.
* Subir esos ficheros a un repositorio vuestro propio en la rama master.
* Acordaros de hacer la configuración para los alias de webpack y jest.
* Crear nueva rama llamada feature/laboratorio-testing-obligatorio.
* Implementar los ejercicios obligatorios.
* Una vez lista la entrega, podéis crear una pull request desde la rama feature/laboratorio-testing-obligatorio hacia la rama master para que se vean los nuevos cambios.
* Por último, entregar el laboratorio en el campus dejando dicha pull request abierta para su corrección.

### Ejericicos

* Añadir tests al mapper ./src/pods/project/project.mapper.ts.
* Añadir tests al componente ./src/common/components/confirmation-dialog/confirmation-dialog.component.tsx.
* Añadir tests al hook ./src/common/components/confirmation-dialog/confirmation-dialog.hook.ts.

__IMPORTANTE__:

* Si es necesario, podéis instalar las librerias que necesitéis.
* Si es necesario, se puede modificar el código original de la implementación, para poder cumplir con las pruebas de testing.
