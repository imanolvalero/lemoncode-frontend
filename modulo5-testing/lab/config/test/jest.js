module.exports = {
  rootDir: '../../',
  preset: 'ts-jest',
  restoreMocks: true,
  testEnvironment: 'jsdom',
  setupFilesAfterEnv: ['<rootDir>/config/test/setup-after.ts'],
  moduleNameMapper: {
    "^core/(.*)": "<rootDir>/src/core/$1",
    "^common/(.*)": "<rootDir>/src/common/$1"
  }
};
