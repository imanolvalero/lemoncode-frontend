export interface Book {
	title: string;
	isRead: boolean;
};

export const isBookRead = (books: Book[], title: string): boolean =>
	books.filter((book) => book.isRead && book.title === title).length > 0;
