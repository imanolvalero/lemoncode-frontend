const concat = (a,b) => [...a,...b]
const concatMultiple = (arr, ...rest) =>
	rest.length === 0 ? arr : [...arr, ...concatMultiple(...rest)];

module.exports = { concat, concatMultiple }