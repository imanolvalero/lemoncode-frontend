import "jest";
import { SlotMachine } from "./slotMachine";

describe("SlotMachine Test", () => {
	it("Check play Test", () => {
        const consoleOutput = jest.spyOn(global.console, 'log')
        const machine =  new SlotMachine();
        let playedTimes: number = 0
        let output = ''

        while (true){
            machine.play()
            output = consoleOutput.mock.calls[playedTimes][0]
            playedTimes++
            if (output.startsWith('Congratulations')){
                expect(parseInt(output.split(' ')[3])).toBe(playedTimes)
                break
            }
        }
	});
});
