const { concat, concatMultiple } = require('./concat');

describe("Concat tests", () => {
    const [arr1, arr2, arr3] = [[1,2,3],[4,5,6],[7,8,9]]
    
	it("Concat Test", () => {
        const output = [1,2,3,4,5,6]
        expect(concat(arr1, arr2)).toEqual(output)
    });

	it("ConcatMultiple Test", () => {
        const output = [1,2,3,4,5,6,7,8,9]
        expect(concatMultiple(arr1, arr2, arr3)).toEqual(output)
    });
});
