const { clone, merge } = require('./cloneMerge');

describe("Clone Merge tests", () => {
    const a = { name: "Maria", surname: "Ibañez", country: "SPA" };
    const b = { name: "Luisa", age: 31, married: true };
    
	it("Clone Test", () => {
        const output = clone(a)

        expect(a).not.toBe(output)
        expect(a).toEqual(output)
    });

	it("Merge Test", () => {
        const output = {
            name: "Maria", 
            age: 31, 
            married: true, 
            surname: "Ibañez", 
            country: "SPA"
        }
        expect(merge(a, b)).toEqual(output)
    });
});
