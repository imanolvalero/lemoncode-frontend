const head = ([first, ..._ ]) => first
const tail = ([_, ...rest]) => rest
const init = ([...arr]) => tail(arr.reverse()).reverse()
const last = ([...arr]) => head(arr.reverse())

module.exports = { head, tail, init, last }