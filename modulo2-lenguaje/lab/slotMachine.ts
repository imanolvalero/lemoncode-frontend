export class SlotMachine {
	private _cash: number;
	private _reelNumber: number;

	constructor(reelNumber: number = 3, initCash: number = 0) {
		this._reelNumber = reelNumber;
		this._cash = initCash;
	}

	private getRandom = (seed: number): boolean => 
        !!(Math.round(Math.random() * (seed + 1)) % 2);

	private getThrow = (num: number): boolean[] => 
        [...Array(num)].map((_, idx) => this.getRandom(idx));

	private _checkThrow = (arr: boolean[]): boolean => 
        arr.reduce((ret, val) => ret && val, true);

	private _showMessage = (result: boolean): string =>
		result 
            ? `Congratulations!!!. You won ${this._cash} coins!!` 
            : `Good luck next time!!`;

	private _givePrice = (): number => (this._cash = 0);

	play() {
		this._cash++;
		let result = this._checkThrow(this.getThrow(this._reelNumber));
		console.log(this._showMessage(result));
		if (result) this._givePrice();
	}
}
