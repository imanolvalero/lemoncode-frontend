const { head, tail, init, last } = require('./arrayOperations');

describe("Array operations tests", () => {
    const input = [1,2,3,4]

	it("Head Test", () => {expect(head(input)).toEqual(1)});
	it("Tail Test", () => {expect(tail(input)).toEqual([2,3,4])});
	it("Init Test", () => {expect(init(input)).toEqual([1,2,3])});
	it("Last Test", () => {expect(last(input)).toEqual(4)});
});
