import "jest";
import { Book, isBookRead } from "./readBooks";

describe("ReadBooks Test", () => {
	const books: Book[] = [
		{ title: "Harry Potter y la piedra filosofal", isRead: true },
		{ title: "Canción de hielo y fuego", isRead: false },
		{ title: "Devastación", isRead: true },
	];

	it("isBookRead Test", () => {
		expect(isBookRead(books, "Devastación")).toBe(true);
		expect(isBookRead(books, "Canción de hielo y fuego")).toBe(false);
		expect(isBookRead(books, "Los Pilares de la Tierra")).toBe(false);
	});
});
