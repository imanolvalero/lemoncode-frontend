export const set = (orig: object, ...args: any[]) => 
    ({...orig, ...Object.fromEntries(Array(args))})

export const currySet = (name: string): Function =>
    (orig: object, val:any) => ({...orig, [name]: val})
