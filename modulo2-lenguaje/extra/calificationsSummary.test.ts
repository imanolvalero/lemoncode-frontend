import "jest";
import {
	summarizeClassRoom,
	Student,
	StudentSumary,
} from "./calificationsSummary";

describe("CalificationsSummary problem", () => {
	test("Test", () => {
		const input: Student[]= [
			{
				name: "Juan",
				califications: [
					1.56, 2.13, 7.53, 9.71, 2.67, 2.43, 2.86, 9.42, 8.08, 7.34,
				],
			},
			{
				name: "Álvaro",
				califications: [
					4.49, 1.52, 7.0, 8.3, 8.01, 6.45, 3.72, 3.27, 6.99, 6.01,
				],
			},
			{
				name: "María",
				califications: [
					2.99, 7.33, 1.14, 3.26, 0.98, 2.94, 4.99, 4.51, 1.8, 9.3,
				],
			},
			{
				name: "Jorge",
				califications: [
					4.6, 3.63, 9.07, 9.03, 3.05, 6.61, 4.81, 1.39, 2.97, 8.69,
				],
			},
			{
				name: "Mónica",
				califications: [
					9.72, 6.07, 1.11, 4.72, 0.04, 1.56, 0.66, 3.87, 6.97, 9.48,
				],
			},
		];
		const output: StudentSumary[] = [
			{ name: "Juan", highest: 9.71, lowest: 1.56, mean: 5.37 },
			{ name: "Álvaro", highest: 8.3, lowest: 1.52, mean: 5.58 },
			{ name: "María", highest: 9.30, lowest: 0.98, mean: 3.92 },
			{ name: "Jorge", highest:  9.07, lowest: 1.39, mean: 5.38 },
			{ name: "Mónica", highest: 9.72, lowest: 0.04, mean: 4.42 },
		];
		expect(summarizeClassRoom(input)).toStrictEqual(output);
	});
});
