import "jest";
import { set, currySet } from "./currySetter";

describe("Curry Setter problem", () => {
	it("Regular Set test", () => {
		const input = { name: "Julia", surname: "Álvarez", age: 19 };
		const updatedInput = { name: "Julia", surname: "Álvarez", age: 25 };
		const output = set(input, "age", 25);
		expect(updatedInput).not.toStrictEqual(input);
		expect(updatedInput).toStrictEqual(output);
	});

	it("Curry Set test", () => {
		const input = { name: "Julia", surname: "Álvarez", age: 19 };
		[
			["name", "Ana", { name: "Ana", surname: "Álvarez", age: 19 }],
			["surname", "Gonz", { name: "Julia", surname: "Gonz", age: 19 }],
			["age", 25, { name: "Julia", surname: "Álvarez", age: 25 }],
		].forEach(([property, value, output]) =>
			expect(currySet("" + property)(input, value)).toStrictEqual(output)
		);
	});
});
