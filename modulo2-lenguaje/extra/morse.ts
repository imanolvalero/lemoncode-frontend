const morseAlphabet: object = {
	"0": "-----",
	"1": ".----",
	"2": "..---",
	"3": "...--",
	"4": "....-",
	"5": ".....",
	"6": "-....",
	"7": "--...",
	"8": "---..",
	"9": "----.",
	a: ".-",
	b: "-...",
	c: "-.-.",
	d: "-..",
	e: ".",
	f: "..-.",
	g: "--.",
	h: "....",
	i: "..",
	j: ".---",
	k: "-.-",
	l: ".-..",
	m: "--",
	n: "-.",
	o: "---",
	p: ".--.",
	q: "--.-",
	r: ".-.",
	s: "...",
	t: "-",
	u: "..-",
	v: "...-",
	w: ".--",
	x: "-..-",
	y: "-.--",
	z: "--..",
	".": ".-.-.-",
	",": "--..--",
	"?": "..--..",
	"!": "-.-.--",
	"-": "-....-",
	"/": "-..-.",
	"@": ".--.-.",
	"(": "-.--.",
	")": "-.--.-",
};
enum Signal {OFF = '0', ON = '1'}
const BeatsBetweenWords: number = 7;
const BeatsBetweenChars: number = 3;
const BeatsBetweenSignals: number = 1;
const DotBeats: number = 1;
const DashBeats: number = 3;
const SignalBeats: Map<string, number> = new Map<string, number>(
	[[".", DotBeats],["-", DashBeats]]
);

const morseToSignal = (morseChar:string):string =>
			Signal.ON.repeat(SignalBeats.get(morseChar) ?? 0)

const Signals: Map<string, string> = new Map<string, string>(
	Object.entries(morseAlphabet).map(([key, signals]:[string, string]) => [
		key,
		signals.split('').map(morseToSignal)
			.join(Signal.OFF.repeat(BeatsBetweenSignals))
	])
);

const charToSignal = (char: string):string => Signals.get(char) ?? ''

const wordToSignal = (word: string):string =>
	word.split('').map(charToSignal).join(Signal.OFF.repeat(BeatsBetweenChars))

const phraseToSignal = (phrase: string):string => 
	phrase.toLocaleLowerCase().split(' ').map(wordToSignal)
		.join(Signal.OFF.repeat(BeatsBetweenWords)) + Signal.OFF


export class MorseMachine {
	private _finishMessage: string;
	private _beatDuration: number;
	private _transmitFunction: Function;
	private _outputFunction: Function;
	private _queue: number[];

	constructor({
		finishMessage = 'Message transmited', 
		beatDuration = 50,
		transmitFunction = console.log,
		outputFunction = console.log
	}: {finishMessage?: string;
		beatDuration?: number;
		transmitFunction?:Function;
		outputFunction?:Function;
	} = {}) 
	{
		this._finishMessage = finishMessage 
		this._beatDuration = beatDuration;
		this._transmitFunction = transmitFunction;
		this._outputFunction = outputFunction;
		this._queue = new Array(0);
	}

	private async _write() {
		if (this._queue.length) {
			this._transmitFunction(this._queue.shift());
			await setTimeout(() => this._write.call(this), this._beatDuration);
		} else this._outputFunction(this._finishMessage)
	}

	send(phrase: string): void {
		this._queue = phraseToSignal(phrase).split('').map(x => parseInt(x));
		this._write();
	}
}