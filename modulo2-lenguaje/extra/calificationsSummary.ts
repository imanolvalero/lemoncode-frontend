Number.prototype.toPrecision(3);

export interface Student {
	name: string;
	califications: number[];
}

export interface StudentSumary {
	name: string;
	lowest: number;
	highest: number;
	mean: number;
}
const fix = (num: number, decimals: number): number =>
	parseFloat(num.toFixed(decimals));

const mean = (nums: number[]): number =>
	fix(
		nums.reduce((ret: number, num: number) => (ret += num), 0) /
			nums.length,
		2
	);

export const summarizeClassRoom = (studentList: Student[]): StudentSumary[] =>
	studentList.map(({ name, califications }) => ({
		name,
		lowest: Math.min(...califications),
		highest: Math.max(...califications),
		mean: mean(califications),
	}));
