import "jest";
import { MorseMachine } from "./morse";

describe("Morse problem", () => {
	afterEach(() => {         
        jest.useRealTimers(); 
    });

	it("Simple test", async () => {
		// taken from https://en.wikipedia.org/wiki/Morse_code
		const finishMessage: string = 'FIN'
		const beatDuration: number = 50;
		const transmitFunction: jest.Mock = jest.fn();
		const outputFunction: jest.Mock = jest.fn();
		
		const input: string = "MORSE CODE";
		const codes: string = 
			"111011100011101110111000101110100010101000100000001110101110100011101110111000111010100010";
		const output: any = codes.split('').map(x => [parseInt(x)])
		
		jest.useFakeTimers(); 

		const morse: MorseMachine = new MorseMachine(
			{finishMessage, beatDuration, transmitFunction, outputFunction}
		);
		morse.send(input);
		jest.advanceTimersByTime(beatDuration * codes.length * 1.1)
		
		expect(transmitFunction.mock.calls).toEqual(output);
		expect(outputFunction.mock.calls[0][0]).toEqual(finishMessage);
	});
});
