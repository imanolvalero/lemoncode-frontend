class OriginalReminder {
    constructor(text) {
        this.text = text;
    }
    /*
    Esta implementacion falla porque en el string template, this contiene la 
    referencia al primer objeto global, el cual no tiene la propiedad text
    */
    remindMe(delay) {
        setTimeout(function () {
            console.log(
                `Your reminder after ${delay} seconds is: ${this.text}`
            );
        }, delay * 1000);
    }
}

class FixedReminder {
    constructor(text) {
        this.text = text;
    }
    /*
    se soluciona almacenando el contenido de this.text en una variable
    text en el contexto de timeout, para que se encole junto a la funcion
    a invocar con retaro, por closure
    */
    remindMe(delay) {
        let text = this.text;
        setTimeout(function () {
            console.log(`Your reminder after ${delay} seconds is: ${text}`);
        }, delay * 1000);
    }
}

module.exports = { OriginalReminder, FixedReminder }
