const getPlayersOrder = (players, turns) => 
    [...Array(turns).keys()]
    .reduce(([head, ...tail]) => [...tail, head], players)

module.exports = getPlayersOrder