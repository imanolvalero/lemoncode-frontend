const f = require('./args');

describe('Args test', () => {
    test('Apartado A', () => {
        const output = [[2], ['JS rocks!', true], ['b', false], [100, false]]
        console.log = jest.fn();
        f("JS rocks!", { b: "b"});
        expect(console.log.mock.calls).toEqual(output);
    });

    test('Apartado B', () => {
     const output = [[1], [{ b: "b" }, true], [undefined, true], [100, false]]
        console.log = jest.fn();
        f({ b: "b" });
        expect(console.log.mock.calls).toEqual(output);
    });

    test('Apartado C', () => {
        const error = new TypeError("Cannot destructure property 'b' of " 
            + "'(intermediate value)(intermediate value)(intermediate value)'"
            + " as it is null.")
        expect(() => f("JS sucks!", null, 13)).toThrowError(error);
    });
});
