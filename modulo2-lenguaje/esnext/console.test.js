const { fA, fB } = require("./console");

describe("Console test", () => {
    afterEach(() => {
        jest.useRealTimers();
    });
    test("Apartado A", () => {
        const output = [
            [1, 3],
            [1, 2],
            [1, 2],
        ];
        console.log = jest.fn();
        fA();
        expect(console.log.mock.calls).toEqual(output);
    });

    test("Apartado B", () => {
        const output = [[1, 3], [1, 3], [1, 2], [5], [5, 6], [1, 2]];
        console.log = jest.fn();
        fB();
        expect(console.log.mock.calls).toEqual(output);
    });
});
