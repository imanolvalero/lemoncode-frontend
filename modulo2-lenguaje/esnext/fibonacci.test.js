const fib = require("./fibonacci");

describe("Fibonacci function", () => {
    [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144]
    .forEach((num, idx) =>  
        test(`Fib(${idx})`, () => {
            expect(fib(idx)).toBe(num);
        })
    )
});