const swap = require("./swap");

describe("Swap problem", () => {
    test(`Test`, () => {
        const inputA = 'A'
        const inputB = 'B'
        console.log = jest.fn();
        swap(inputA, inputB);
        expect(console.log.mock.calls[0][0]).toEqual('You did it!');
    })
});

