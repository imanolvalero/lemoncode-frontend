const swap = (a, b) => {
    const orig_a = a;
    const orig_b = b;

    [a, b] = [b, a];

    console.log(a === orig_b && b === orig_a ? "You did it!" : "Keep trying!");
};

module.exports = swap;
