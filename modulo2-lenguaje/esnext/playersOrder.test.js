const  getPlayersOrder = require("./playersOrder");

describe("playersOrder problem", () => {
    test(`Test`, () => {
        const players = ["Ana", "Juan", "Pablo", "Lucia"]
        const turns = 2
        const output = ["Pablo", "Lucia", "Ana", "Juan"]
        expect(getPlayersOrder(players,turns)).toStrictEqual(output);
    })
});