# Master Front End XI - Modulo 2 - Lenguaje

## Ejercicios ESNext

### Args

Dada la siguiente función:

```javascript
function f(a, { b } = {}, c = 100) {
  console.log(arguments.length);
  console.log(a, a === arguments[0]);
  console.log(b, b === arguments[1]);
  console.log(c, c === arguments[2]);
}
```

#### Apartado A

¿Qué muestra por consola esta llamada?

```javascript
f("JS rocks!", { b: "b" });
```

#### Apartado B

¿Y con estos argumentos?

```javascript
f({ b: "b" });
```

#### Apartado C

¿Y ahora?

```javascript
f("JS sucks!", null, 13);
```

### Console

#### Apartado A

Intenta razonar cual será el resultado de la consola al ejecutar este código:

```javascript
var a = 1;
let b = 2;

{
  try {
    console.log(a, b);
  } catch (error) {}
  let b = 3;
  console.log(a, b);
}

console.log(a, b);

() => {
  console.log(a);
  var a = 5;
  let b = 6;
  console.log(a, b);
};

console.log(a, b);
```

#### Apartado B

Sin tocar ninguno de los `console.log` anteriores, modifica ligeramente el código para que muestre la siguiente secuencia:

```
1 3
1 3
1 2
5
5 6
1 2
```

### Fibonacci

Implementa una función para calcular el n-enésimo termino de la sucesión de Fibonacci. Esta sucesión tiene sus dos primeros términos prefijados:

```
fib(0) = 0
fib(1) = 1
```

Y a partir de aqui, el siguiente término se calcula a partir de los dos anteriores:

```
fib(2) = fib(1) + fib(0)
...
fib(n + 1) = fib(n) + fib(n - 1)
```

**TIP**: Es el clásico problema donde el término siguiente depende del actual y el anterior.

**TIP**: También se puede abordar con recursividad, pero buscamos una solución iterativa para hacer uso de destructuring con múltiples asignaciones.

```javascript
const fib = n => {
  /* Implementation here*/
};
```

### Players Order

En una gran cantidad de juegos el sistema de turnos es sencillo, una vez el jugador actual ha consumido su turno, pasa al final de la cola y le toca al siguiente. Dada una lista inicial de jugadores, implementa una función que devuelva la nueva lista de jugadores, en el orden correcto, después de X turnos.

**TIP**: Aunque se puede resolver con el operador `%`, intenta idear una solución usando spread/rest y destructuring.

```javascript
const getPlayersOrder = (players, turns) => {
  /* Implementation here */
};

// Un ejemplo:
const newOrderIn2Turns = getPlayersOrder(["Ana", "Juan", "Pablo", "Lucia"], 2);
console.log(newOrderIn2Turns); // ["Pablo", "Lucia", "Ana", "Juan"]
```

### Reminder

El siguiente código implementa una sencilla clase que actúa como reminder, es decir, dado un mensaje, lo muestra por consola transcurrido (al menos) el tiempo indicado por el usuario:

```javascript
class Reminder {
  constructor(text) {
    this.text = text;
  }

  remindMe(delay) {
    setTimeout(function() {
      console.log(`Your reminder after ${delay} seconds is: ${this.text}`);
    }, delay * 1000);
  }
}
```
Te animamos a que crees una nueva instancia de reminder y la utilices. Escribe el mensaje que tu quieras y añade unos pocos segundos de retardo.
Comprueba la salida por consola ... algo no funciona como esperábamos ¿verdad? ¿Sabrías decirnos que está pasando aquí? ¿Cómo lo arreglarías?

### Swap

¿Sabrías intercambiar el valor de estas 2 variables en una sola línea?

```javascript
let a = "A";
let b = "B";

// Implementation here, one line, one shot!

console.log(a === "B" && b === "A" ? "You did it!" : "Keep trying!");
```

