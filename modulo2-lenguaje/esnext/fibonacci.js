const fib = n => [...Array(n).keys()].reduce(([act, next]) => 
    [act, next] = [next, act + next], [0,1])[0]

module.exports = fib
