const { OriginalReminder, FixedReminder } = require("./reminder");

describe("Reminder problem", () => {
	afterEach(() => {
		jest.useRealTimers();
	});

	test(`Wrong reminder test`, () => {
		const msg = "message";
		const delay = 2;
        const error = []

		console.log = jest.fn();
 
		const reminder = new OriginalReminder(msg);
		expect(reminder.remindMe(msg)).toBe(undefined);

 		expect(console.log.mock.calls).toEqual(error)
	});

	test(`Fixed reminder test`, () => {
		const msg = "message";
		const delay = 2;
		const output = `Your reminder after ${delay} seconds is: ${msg}`;

		console.log = jest.fn();
		jest.useFakeTimers();

		const reminder = new FixedReminder(msg);
		reminder.remindMe(delay);

		jest.advanceTimersByTime(delay * 2000);
		expect(console.log.mock.calls[0][0]).toEqual(output);
	});
});
