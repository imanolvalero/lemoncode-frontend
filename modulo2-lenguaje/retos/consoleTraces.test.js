const traces = require("./consoleTraces");

describe("consoleTraces Test", () => {
    afterEach(() => {         
        jest.useRealTimers(); 
    });
    
    test("Orden de salida por consola", async () => {
        const delay = 1000;
        const output = [['third'],['second'],['first']];

        //jest.useFakeTimers();
        console.log = jest.fn();
        console['label'] = 'caracola'

        await traces();

        //jest.advanceTimersByTime(delay); 

        expect(console.log.mock.calls).toEqual(output);
        //jest.useRealTimers(); 
    });
});
