import "jest";
const { TreeNode } = require("./tree");

describe("Tree Test", () => {
	it("Test", () => {
        const output: string = 'TreeNode: 1 [TreeNode: 2 [],TreeNode: 3 [],TreeNode: 4 []]'
        const tree = new TreeNode(1, [2,3,4])
		expect(tree.toString()).toEqual(output)
	});
});
