const traces = async () => {
  const delay = ms => new Promise(resolve => setTimeout(resolve, ms));

  const showMessage = async ([time, message]) => {
    await delay(time);
    console.log(message);
  };

  const triggers = [
    async () => await showMessage([2000, "third"]),
    async () => await showMessage([1000 , "second"]),
  ];

  const run = async triggers => {
    for( let index in triggers ) {
      await triggers[index]();
    }
    console.log("first");
  };
  await run(triggers);
}

module.exports = traces;
