const { deepGet, deepSet } = require("./deepAccess");

describe("deepAccess Test", () => {
	it("Apartado A: deepGet", () => {
		const myObject = {
			a: 1,
			b: {
				c: null,
				d: {
					e: 3,
					f: {
						g: "bingo",
					},
				},
			},
		};

		expect(deepGet(myObject, "x")).toEqual(undefined);
		expect(deepGet(myObject, "a")).toEqual(1);
		expect(deepGet(myObject, "b")).toEqual({ c: null, d: { e: 3, f: { g: "bingo" } } });
		expect(deepGet(myObject, "b", "c")).toEqual(null);
		expect(deepGet(myObject, "b", "d", "f", "g")).toEqual("bingo");
		expect(deepGet(myObject)).toEqual(myObject);
	});

	it("Apartado B: deepSet", () => {
		const myObject = {};

		deepSet(1, myObject, "a", "b");
		expect(myObject).toEqual({ a: { b: 1 } });

		deepSet(2, myObject, "a", "c");
		expect(myObject).toEqual({ a: { b: 1, c: 2 } });

		deepSet(3, myObject, "a");
		expect(myObject).toEqual({ a: 3 });

		deepSet(4, myObject);
		expect(myObject).toEqual({ a: 3 });
	});
});
