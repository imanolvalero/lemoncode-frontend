import "jest";
import { memoizeA, memoizeB, memoizeC } from "./memoization";

describe("Memoization problem", () => {
  for (const [apartado, fn] of Object.entries({'A':memoizeA, 'B':memoizeB})) {
    it("Apartado " + apartado, () => {
      let count = 0;
      const expensiveFunction = (): number => {
        count++;
        return 3.1415;
      }
      const output: number = 3.1415;
  
      const memoized = fn(expensiveFunction);
  
      expect(memoized()).toBe(output);
      expect(memoized()).toBe(output);
      expect(memoized()).toBe(output);
      expect(count).toBe(1);
    });
  }

  it("Apartado C", () => {
    let count = 0;
    const repeatText = (repetitions: number, text: string): string =>
      (count++, `${text} `.repeat(repetitions).trim())
    
    const memoizedGreet = memoizeC(repeatText);
    
    expect(memoizedGreet(1, "pam")).toBe('pam');
    expect(memoizedGreet(3, "chun")).toBe('chun chun chun');
    expect(memoizedGreet(1, "pam")).toBe('pam');
    expect(memoizedGreet(3, "chun")).toBe('chun chun chun');
    expect(count).toBe(2);
	});

});
