const deepGet = (obj, ...route) => route.reduce((res, key) => res[key], obj);

const deepSet = (val, obj, ...route) => {
	let [tmp, lastIndex] = [obj, route.length - 1]
  route.forEach((key, index)  => {
    if (index === lastIndex) tmp[key] = val
    else {
      tmp[key] = {...tmp[key]} ?? {}
      tmp = tmp[key]
    }
  })
};


module.exports = { deepGet, deepSet };