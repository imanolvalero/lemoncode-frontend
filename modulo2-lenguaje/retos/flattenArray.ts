export const flatten = (arr: unknown[]) => {
    let tmp: unknown[] = [].concat.apply([], arr as never[])
    while (tmp.some(Array.isArray)) tmp = [].concat.apply([], tmp as never[])
    return tmp
}

export const flattenES10 = (arr:unknown[]) => arr.flat(Infinity)
