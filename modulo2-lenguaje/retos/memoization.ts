export const memoizeA = (fn: Function): Function => {
	let cache = null;
	return () => (cache = cache || fn());
};

export const memoizeB = f => {let x = null; return () => (x = x || f());};

export const memoizeC = (fn: Function, ...args: unknown[]): Function => {
	let cache = new Map<unknown, unknown>();
	return (...args) => cache.get("" + args) 
                   ?? cache.set("" + args, fn(...args)).get("" + args);
};
