export class TreeNode <T>{
    private _value: T;
    private _leafs: TreeNode<T>[];

    constructor(value:T, leafs:T[] = [] ){
        this._value = value
        this._leafs = leafs.map(x => new TreeNode<T>(x))
    }

    public get value(): T { return this._value }
    public set value(value: T) { this._value = value }

    has(value: T): boolean { return this._leafs.some(leaf => leaf.value === value) }
    get(value: T): TreeNode<T> { return this._leafs.filter(leaf => leaf.value === value)[0] }
    add(value: T): void {this._leafs.push(new TreeNode<T>(value))}
    del(value: T): void {this._leafs.splice(this._leafs.indexOf(this.get(value)), 1)}
   
    toString(): string {
        return `TreeNode: ${this._value} [${this._leafs.map(x => ''+x).join(',')}]`
    }
}
