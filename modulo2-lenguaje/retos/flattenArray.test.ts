import "jest";
const { flatten } = require("./flattenArray");

describe("flattenArray Test", () => {
	it("Test", () => {
		const sample = [1, [2, 3], [[4], [5, 6, [7, 8, [9]]]]];
		const output = [1, 2, 3, 4, 5, 6, 7, 8, 9];

		expect(flatten(sample)).toEqual(output)
	});
});
