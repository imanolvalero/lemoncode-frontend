function showPrimes(from, to) {
    [...Array(to - from + 1).keys()].map(pos => 
        console.log(
            `${pos+from} is ${isPrime(pos+from)? 'PRIME!' : 'not a prime'}` 
        )
    )
}

function isPrime(num) {
    if (num === 1) return false
    let [_,__, ...range] = [...Array(num).keys()]
    
    return range.every(val => num % val !== 0)
}

module.exports = showPrimes