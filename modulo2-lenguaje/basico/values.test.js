const values = require('./values.js')

describe("values function", () => {
    test("Test output values", () => {
        const input = {
            id: 31, duration: 310, name: "long video", format: "mp4"
        }
        const output = [31, 310, "long video", "mp4"]
        expect(values(input)).toEqual(output);
    });
});