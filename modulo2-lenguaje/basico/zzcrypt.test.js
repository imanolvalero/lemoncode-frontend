const decrypt = require("./zzcrypt");

describe("decrypt function", () => {
    test("test", () => {
        const input =
            "': rg!qg yq,urae: ghsrf wuran shrerg jq,u'qf ra r' ,qaq' er g'q,o rg,fuwurae: m!hfua( t'usqfuq ,:apu(:m xv";
        const output =
            "lo estas haciendo super bien, puedes vacilar en el canal de slack escribiendo 'turing fliparia conmigo' :)";
        expect(decrypt(input)).toEqual(output);
    });
});