function values(obj) {
    return Object.keys(obj).map( key => obj[key])
}

function getMean(values) {    
    return values.reduce((ret,val) => ret + val, 0) / values.length
}

function getCalificationText(value) {
    return [
        [ 10 , 'Matrícula de Honor' ],
        [ 9 , 'Sobresalienter' ],
        [ 7 , 'Notable' ],
        [ 6 , 'Bien' ],
        [ 5 , 'Suficiente' ],
        [ 4 , 'Insuficiente' ],
        [ 0 , 'Muy deficiente' ]
    ].reduce(
        (ret, val) => ret == '' && value >= val[0] ? val[1]: ret,
        ''
    )
}

function printAverage(classResults) {
    console.log(getCalificationText(getMean(values(classResults))))
}

module.exports = printAverage
