const zipObjects = require('./zip')

describe("zipObject function", () => {
    test("keys and values with same lenght", () => {
        const keys = ["spanish", "english", "french"]
        const values = ["hola", "hi", "salut"]
        const output = {spanish: "hola", english: "hi", french: "salut"}
        expect(zipObjects(keys, values)).toEqual(output);
    });

    test("Keys with less items than values", () => {
        const keys = ["spanish"]
        const values = ["hola", "hi", "salut"]
        const output = {spanish: "hola",}
        expect(zipObjects(keys, values)).toEqual(output);
    });

    test("Keys with more items than values", () => {
        const keys = ["spanish", "english", "french"]
        const values = ["hola"]
        const output = {spanish: "hola", english: undefined, french: undefined}
        expect(zipObjects(keys, values)).toEqual(output);
    });
});
