const subsets = require('./subsets');

describe('subsets test', () => {
    test('Test', () => {
        const input = 'message';
        const output = ['essage', 'ssage', 'sage', 'age', 'ge', 'e'];
        expect(subsets(input)).toStrictEqual(output);
    });
});