const includes = require("./includes");

describe("ES5 Includes test", () => {

    test("Test function", () => {
        const input = [1 ,2, 3];

        expect(includes(input, 3)).toBe(true);
    expect(includes(input, 0)).toBe(false);
    });

});
