function subsets(word) {
    return [...Array(word.length).keys()]
        .filter(pos => pos > 0)
        .map(pos => pos && word.substring(pos))
}

module.exports = subsets;
