function zipObject(keys, values) {
    return Object.fromEntries(
        keys.map((val, idx) => [val, values?.[idx] ?? undefined])
    );
}

function decrypt(secret) {
    let plain = "abcdefghijklmnopqrstuvwxyz:()!¡,' ";
    let cipher = "qw,ert(yuio'pa:sdfg!hjklz¡xcv)bnm ";
    let dict = zipObject(cipher.split(''), plain.split(''))
    return secret.split('').map(l => dict[l]).join('')
}

module.exports = decrypt
