function includes(array, value) {
    return array.indexOf(value) > -1
}

module.exports = includes