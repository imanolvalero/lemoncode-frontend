function DiceGameFactory(dices = 2, faces = 6) {
    let values = new Array(dices);

    const game = {
        throwDices: function () {
            values = values.map(_ => parseInt(Math.random() * faces));
            console.log(
                "Has sacado: [" + values.map((val) => val + 1).join(" | ") + "]"
            );
            if (
                values.every(val => val === (faces - 1))
            )
                console.log("Premio!!");
        },
        reset: function () {
            values = values.fill(null)
            console.log("Tira los dados y prueba suerte");
        },
        getValues: function () {
            return values.map(val => val === null ? null : val + 1);
        },
    };
    game.reset();
    return game;
}

module.exports = DiceGameFactory;

let game = DiceGameFactory(1,2)
game.throwDices()
game.reset()
