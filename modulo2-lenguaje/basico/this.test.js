const reverseText = require('./reverseText');

describe('reverseText test', () => {
    test('Test', () => {
        const input = 'Uno dos tres';
        const output = 'tres dos Uno';
        console.log = jest.fn();
        reverseText(input);
        expect(console.log.mock.calls[0][0]).toEqual(output);
    });
});
