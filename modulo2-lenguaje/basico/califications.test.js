const printAverage = require('./califications.js')

describe("printAverage function", () => {
    it("Prints alification 'Notable'", () => {
        const input = {
            David: 8.25,
            Maria: 9.5,
            Jose: 6.75,
            Juan: 5.5,
            Blanca: 7.75,
            Carmen: 8,
        }
        const output = 'Notable'
        console.log = jest.fn();
        printAverage(input)
        expect(console.log.mock.calls[0][0]).toEqual(output);
    });
});
