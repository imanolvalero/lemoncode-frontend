const { fA, fB, fC } = require("./hoisting");

describe("Hoisting test", () => {

    test("Apartado A", () => {
        const output = [[undefined],[undefined],['good job!']];
        console.log = jest.fn();
        fA()
        expect(console.log.mock.calls).toEqual(output);
    });

    test("Apartado B", () => {
        const output = new ReferenceError('c is not defined');
        expect(fB).toThrowError(output);
    });
  
    test("Apartado C", () => {
        const output = new ReferenceError('c is not defined');
        expect(fC).toThrowError(output);
    });
  
});
