const { isEqual, isDeepEqual } = require("./deepEqual");

describe("Equality functions", () => {
    test("Apartado A: isEqual", () => {
        const inputA = { name: "María", age: 30 };
        const inputB = { name: "María", age: 30 };
        const output = true
        expect(isEqual(inputA, inputB)).toBe(output);
        
    });
    test("Apartado B: isDeepEqual", () => {
        const inputA = {
            name: "María",
            age: 30,
            address: { city: "Málaga", code: 29620 },
            friends: ["Juan"],
        };
        const inputB = {
            name: "María",
            age: 30,
            address: { city: "Málaga", code: 29620 },
            friends: ["Juan"],
        };
        const output = true
        expect(isDeepEqual(inputA, inputB)).toBe(output);
    });
});
