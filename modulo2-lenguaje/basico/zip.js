function zipObject(keys, values) {
    return Object.fromEntries(
        keys.map((val, idx) => [val, values?.[idx] ?? undefined])
    );
}

module.exports = zipObject