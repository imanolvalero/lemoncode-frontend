function f() {
  console.log(a);
  console.log(g());

  var a = "good job!";
  function g() {
    return a;
  }
  console.log(a);
}

function fA() {
    var a;
    console.log(a);
    console.log(g());

    a = "good job!";
    function g() {
        return a;
    }
    console.log(a);
}

function fB() {
    var a = 1;

    (function () {
        var a, c;
        console.log(a);
        a = 2;
        b = 4;
        c = 3;
    })();

    console.log(a);
    console.log(b); //this goes to 'window' object
    console.log(c);
}

function fC() {
  var a
    function f() {
    var c;
        console.log(a);
        b = 4; //this goes to 'window' object
        c = 3;
    }
    
  f();
    a = 1;
    console.log(a);
    console.log(b);
    console.log(c);
}
module.exports = { fA, fB, fC };
