const { clone, merge } = require("./cloneMerge.js");

describe("clone and Merge functions", () => {
    test("Apartado A: clone", () => {
        const input = { name: "Luisa", age: 31, married: true };
        const output = { name: "Luisa", age: 31, married: true };
        expect(clone(input)).toEqual(output);
    });
    test("Apartado B: merge", () => {
        const source = { name: "Luisa", age: 31, married: true };
        const target = { name: "Maria", surname: "Ibañez", country: "SPA" };
        const output = {
            name: "Maria",
            age: 31,
            married: true,
            surname: "Ibañez",
            country: "SPA",
        };
        expect(merge(source, target)).toEqual(output);
    });
});
