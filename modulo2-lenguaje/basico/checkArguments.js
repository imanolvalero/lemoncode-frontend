function f(input = "Unknown") {
    return input ?? '';
}

module.exports = f