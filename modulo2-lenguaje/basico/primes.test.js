const showPrimes = require("./primes");

describe("showPrimes test", () => {
    test("Test function", () => {
        const from = 1;
        const to = 10;
        const output = [
            ["1 is not a prime"],
            ["2 is PRIME!"],
            ["3 is PRIME!"],
            ["4 is not a prime"],
            ["5 is PRIME!"],
            ["6 is not a prime"],
            ["7 is PRIME!"],
            ["8 is not a prime"],
            ["9 is not a prime"],
            ["10 is not a prime"],
        ];
        console.log = jest.fn();
        showPrimes(from, to);
        expect(console.log.mock.calls).toEqual(output);
    });
});
