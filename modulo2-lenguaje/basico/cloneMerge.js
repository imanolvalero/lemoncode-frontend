function clone(source) {
    return {...source}
}

function merge(source, target) {
    return clone({...source, ...target})
}

module.exports = {clone, merge}