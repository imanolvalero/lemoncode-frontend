function biggestWord(phrase) {
    let words = phrase.split(" ");
    let lengths = words.map((word) => word.length);
    let maxlen = Math.max(...lengths);
    let index = lengths.reduce(
        (ret, len, idx) => (ret == -1 && len == maxlen ? idx : ret),
        -1
    );
    return words[index];
}

module.exports = biggestWord;