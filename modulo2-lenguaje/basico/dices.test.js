const DiceGameFactory = require("./dices");

describe("Dice game test", () => {
    const dices = 2;
    const faces = 6;
    const game = DiceGameFactory(dices, faces);

    test("Test Reset", () => {
        const output = new Array(dices).fill(null);
        game.throwDices();
        expect(game.getValues()).not.toBe(output);

        game.reset();
        expect(game.getValues()).toStrictEqual(output);
    });

    test("Use Math.random() for aleatory throws", () => {
        Math.random = jest.fn();

        game.throwDices();
        expect(Math.random).toHaveBeenCalled();
    });

    test("User has been asked for throwing the dices", () => {
        const output = "Tira los dados y prueba suerte";
        console.log = jest.fn();
        let game = new DiceGameFactory(dices, faces);

        expect(console.log.mock.calls[0][0]).toEqual(output);
    });

    test("Print result by console", () => {
        const output = "Tira los dados y prueba suerte";
        console.log = jest.fn();
        const game = DiceGameFactory(1,2)
        expect(console.log.mock.calls[0][0]).toMatch(output);
        game.reset()
        expect(console.log.mock.calls[0][0]).toMatch(output);
    });

    test("Give price on all dices max", () => {
        const output = "Premio!";
        console.log = jest.fn();
        let game = DiceGameFactory(dices, faces);
        for (let i = 0; i < Math.min(faces*dices*100, 200); i++) {
          game.throwDices();
          if (game.getValues().every(dice => dice === faces)) {
            expect(console.log.mock.calls[0][0]).toMatch(output);
            break;
          }
        }
    });
});
