const biggestWord = require('./biggestWord.js')

describe("biggestWord function", () => {
    test("Check on phrase", () => {
        const input = 'Esta frase puede contener muchas palabras'
        const output = 'contener'
        expect(biggestWord(input)).toEqual(output);
    });
    test("Check on other phrase", () => {
        const input = 'Ejercicios básicos de JavaScript'
        const output = 'Ejercicios'
        expect(biggestWord(input)).toEqual(output);
    });
});