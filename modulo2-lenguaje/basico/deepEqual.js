const { moduleExpression } = require("@babel/types")

function isEqual(a, b) {
    const [keysA, keysB] = [Object.keys(a), Object.keys(b)]
    return keysA.length == keysB.length
        && keysA.every(key => a[key] === b[key])
}

function isDeepEqual(a, b) {
    const [keysA, keysB] = [Object.keys(a), Object.keys(b)]
    return keysA.length == keysB.length
        && keysA.every(key => 
            (typeof a[key] === 'object' && typeof b[key] === 'object')
            ? isDeepEqual(a[key], b[key])
            : a[key] === b[key]
        )   
}

module.exports = {isEqual, isDeepEqual}