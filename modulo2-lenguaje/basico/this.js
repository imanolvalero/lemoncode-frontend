var surname = "Pérez";
var person = {
    name: "Juan",
    surname: "González",
    wife: {
        name: "Ana",
        surname: "Jiménez",
        getSurname: function() {
            return this.surname;
        },
    },
};

// lo importante es a qué referncia this en la línea 9
console.log(person.wife.getSurname());          
// Devuelve Jiménez.
// This es el objeto wife del objeto person

var surnameFunction = person.wife.getSurname;   
console.log(surnameFunction());
// En un navegador, devuelve Pérez. La variable suename de la linea 1 se 
// almacena como una propiedad más del objeto window
// En Node, devuelve undefined. Las variables de contexto global no se 
// almacenan como una del objeto global

console.log(surnameFunction.call(person));
// Devuelve González.
// This es el objeto person 
