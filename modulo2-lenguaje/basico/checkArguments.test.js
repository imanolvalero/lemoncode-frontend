const f = require('./checkArguments');

describe("Check arguments", () => {
    test("Test without argument", () => {
        const output = 'Unknown';
        expect(f()).toEqual(output);
    });
    test("Test with undefined argument", () => {
        const input = undefined;
        const output = 'Unknown';
        expect(f(input)).toEqual(output);
    });
    test("Test with null argument", () => {
        const input = null;
        const output = '';
        expect(f(input)).toEqual(output);
    });
    test("Test with a string argument", () => {
        const input = 'string';
        const output = 'string';
        expect(f(input)).toEqual(output);
    });
});
