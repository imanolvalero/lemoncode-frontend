const base = require("./webpack.config.base.js");
const { merge } = require("webpack-merge");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = merge(base, {
	mode: "production",
	module: {
		rules: [
			{
				test: /\.scss$/,
				exclude: /node_modules/,
				use: [
					{loader: MiniCssExtractPlugin.loader},
					{
						loader: "css-loader",
						options: {
							modules: {
								exportLocalsConvention: "camelCase",
							},
						},
					},
					{ loader: "sass-loader" },
				],
			},
		],
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: "styles.[chunkhash:5].css",		
		}),
	],
});
