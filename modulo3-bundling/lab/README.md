# Master Front End XI - Módulo 3 - Bundling Laboratorio básico 

Implementar una aplicación simple que:
* Tenga el bundling montado con webpack.
* Muestre un logo (por ejemplo el de lemoncode u otro que queráis).
* Esté montada con Typescript.
* Muestre el texto "hola mundo" estilado con SASS.

### Pasos para comprobar
Una vez descargados los archivos de este directorio, levantar el entorno con todas sus dependencias:
```
npm ci
```

Para levantar el servidor de desarrollo en http://localhost:8080:
```
npm start
```

Para crear el `bundle` en la carpeta `dist`:
```
npm run build
```
