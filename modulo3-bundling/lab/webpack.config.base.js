const path = require("path");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
	context: path.resolve(__dirname, "src"),
	resolve: {
		extensions: [".js", ".ts"],
	},
	entry: {
		main: "./main.ts",
	},
	output: {
		filename: "[name].[chunkhash:5].js",
		path: path.resolve(__dirname, "dist"),
	},
	module: {
		rules: [
			{
				test: /\.ts$/,
				exclude: /node_modules/,
				loader: "babel-loader",
			},
			{
				test: /\.png$/,
				type: "asset/resource",
			},
		],
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: "./index.html",
			filename: "index.html",
			scriptLoading: "blocking",
		}),
		new CleanWebpackPlugin(),
	],
};
