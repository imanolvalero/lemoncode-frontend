const base = require("./webpack.config.base.js");
const { merge } = require("webpack-merge");
const path = require("path");

module.exports = merge(base, {
	mode: "development",
	module: {
		rules: [
			{
				test: /\.scss$/,
				exclude: /node_modules/,
				use: [
					{ loader: "style-loader" },
					{
						loader: "css-loader",
						options: {
							modules: {
								exportLocalsConvention: "camelCase",
								localIdentName: "[path][name]__[local]--[hash:base64:5]",
								localIdentContext: path.resolve(__dirname, "src/components"),
							},
						},
					},
					{ loader: "sass-loader" },
				],
			},
		],
	},
	devtool: "eval-source-map",
	devServer: {
		port: "8080",
		hot: true,
	},
	stats: "errors-only",
});
