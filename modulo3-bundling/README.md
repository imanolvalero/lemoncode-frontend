# Master Front End XI - Módulo 3 - Bundling Laboratorio

## Básico 
[[Ir a la carpeta]](https://gitlab.com/imanolvalero/lemoncode-frontend/-/tree/master/modulo3-bundling/lab)
Implementar una aplicación simple que:
* Tenga el bundling montado con webpack.
* Muestre un logo (por ejemplo el de lemoncode u otro que queráis).
* Esté montada con Typescript.
* Muestre el texto "hola mundo" estilado con SASS.

## Opcional
[[Ir a la carpeta con la solución con webpack]](https://gitlab.com/imanolvalero/lemoncode-frontend/-/tree/master/modulo3-bundling/opcional/bundling-webpack)
[[Ir a la carpeta con la solución con parcel]](https://gitlab.com/imanolvalero/lemoncode-frontend/-/tree/master/modulo3-bundling/opcional/bundling-parcel)
* Mostrar un hola mundo desarrollado con React.
* Tener una versión de build de producción.
* Tener variables de entorno para diferentes entornos (desarrollo y producción).
* Tener una forma de medir cuanto ocupa cada librería y nuestro código en el bundle.
* Montar lo mismo con parcel (v1).

