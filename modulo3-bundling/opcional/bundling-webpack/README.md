# Master Front End XI - Módulo 3 - Bundling Laboratorio opcional 

## Bundling con `webpack`

* Mostrar un hola mundo desarrollado con React.
* Tener una versión de build de producción.
* Tener variables de entorno para diferentes entornos (desarrollo y producción).
* Tener una forma de medir cuanto ocupa cada librería y nuestro código en el bundle.


### Pasos para comprobar
Una vez descargados los archivos de este directorio, levantar el entorno con todas sus dependencias:
```
npm ci
```

Para levantar el servidor de desarrollo en http://localhost:8080:
```
npm start
```

Para crear el `bundle` en la carpeta `dist`:
```
npm run build
```

Para _pesar_ cada parte del `bundle` en http://localhost:8888:
```
npm run build:perf
```


