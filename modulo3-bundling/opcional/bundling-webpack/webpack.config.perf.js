const prod = require("./webpack.config.prod.js");
const { merge } = require("webpack-merge");
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer'); 

module.exports = merge(prod, {
    plugins: [
        new BundleAnalyzerPlugin(),
    ]
})