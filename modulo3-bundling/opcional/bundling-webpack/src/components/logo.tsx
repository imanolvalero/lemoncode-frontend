import React from "react";
import Logo from "../imgs/logo.png";

export const LogoComponent: React.FC = () => {
    return (
        <img src={Logo} width="300px" />
    );
}