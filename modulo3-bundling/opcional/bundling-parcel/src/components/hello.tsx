import React from "react";
import classes from "../styles.scss";

export const HelloComponent: React.FC = () => {
    return (
        <div className={classes.decoratedString} >
            <h2>Hello from React</h2>
            <p>packed by { process.env.BUNDLER }</p>
        </div>
    );
}