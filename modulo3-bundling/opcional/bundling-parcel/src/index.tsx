import React from 'react';
import ReactDOM from 'react-dom';
import { LogoComponent } from './components/logo';
import { HelloComponent } from './components/hello';

ReactDOM.render(
    <>
        <LogoComponent />
        <HelloComponent />
    </>,
    document.getElementsByTagName('body')[0]
);
