const stringHash = require("string-hash");
const path = require("path");

module.exports = {
    modules: true,
    plugins: {
        "postcss-modules": {
            generateScopedName: function (classname, filename, css) {
                const fn = filename
                    .replace(path.resolve(__dirname, "src"), "") // remove root path
                    .replace(/\.[^/.]+$/, "")                    // remove extension
                    .replaceAll("/", "_");                       // replace slashes by uncescore
                const i = css.indexOf(`.${classname}`);
                const lineNumber = css.substr(0, i).split(/[\r\n]/).length;
                const hash = stringHash(css).toString(36).substr(0, 5);
                return `${fn}_${classname}_${hash}_${lineNumber}`;
            },
            localsConvention: "camelCase",
        },
    },
};
