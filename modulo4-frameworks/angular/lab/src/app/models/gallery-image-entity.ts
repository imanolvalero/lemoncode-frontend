export interface GalleryImageEntity {
    "id":string,
    "created_at":Date,
    "tags":string[]
}
