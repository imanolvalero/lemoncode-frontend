import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-private-menu',
  templateUrl: './private-menu.component.html',
  styleUrls: ['./private-menu.component.scss'],
})
export class PrivateMenuComponent implements OnInit {
  constructor(
    private _authService:AuthService,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this._router.navigate(['/dashboard']);
  }

  logout() {
    this._authService.logout()
  }
}
