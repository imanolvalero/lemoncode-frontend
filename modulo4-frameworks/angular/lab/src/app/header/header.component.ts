import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  isLogged: boolean = false;
  constructor(private _authService:AuthService) { }

  ngOnInit(): void { }

  ngDoCheck() {
    this.isLogged = this._authService.isLogged();
  }
}
