import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { CredentialsEntity } from '../models/credentials-entity';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  cradentials: CredentialsEntity = {
    username: '',
    password: '',
  }
  
  constructor(
    private _auth:AuthService,
    private _router:Router,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {}

  login() {
    console.log('asdasd')
    if (this._auth.login(this.cradentials)) {
      this._router.navigate(['/dashboard']);
    } else {
      this.cradentials = {username: '', password: ''}
      this._snackBar.open('Wrong credentials', 'OK', {duration:2000})
      setTimeout(() => {
        this._snackBar.open('Psss! try master11@lemoncode.net/12345678', 'Thanks!')
      } , 2500)
    }
  }

}
