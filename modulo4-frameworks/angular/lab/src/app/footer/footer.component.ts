import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  username:string = ''
  isLogged:boolean = false
  constructor(private _authService: AuthService) { }

  ngOnInit(): void { }
  ngDoCheck() {
    this.username = this._authService.getUsername()
    this.isLogged = this._authService.isLogged()
  }
}
