import { Injectable } from '@angular/core';
import { CredentialsEntity } from '../models/credentials-entity';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  #currentUsername: string;
  constructor() { 
    this.#currentUsername = window.localStorage.getItem('username') || ''
  }

  #checkCredentials(credentials: CredentialsEntity): boolean {
    const { username, password } = credentials;
    return username === 'master11@lemoncode.net' &&
           password === '12345678';
  }

  login(credentials: CredentialsEntity): boolean {
    const res = this.#checkCredentials(credentials)
    if (res) this.#currentUsername = credentials.username
    window.localStorage.setItem('username', this.#currentUsername)
    return res
  }
  
  logout() {
    console.log('logout')
    this.#currentUsername = ''
    window.localStorage.removeItem('username')
  }

  isLogged(): boolean {
    return !!this.#currentUsername
  }

  getUsername(): string {
    return this.#currentUsername;
  }
}
