import { Component, OnInit } from '@angular/core';
import { GalleryImageEntity } from '../models/gallery-image-entity';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {
  images:GalleryImageEntity[] = []
  position:number= 0
  url:string = ''
  tags:string[] = []
  imagePool:GalleryImageEntity[] = []
  imagePoolLength:number = 7

  constructor() { 
    fetch('https://cataas.com/api/cats')
    .then(res => res.json())
    .then(data => {
      this.images = data 
      this.position = 0
      this.updatePool()
    })
  }

  ngOnInit(): void { }

  imageUrl(image:GalleryImageEntity) {
    return `https://cataas.com/cat/${image.id}`
  }
  
  setImage(image:GalleryImageEntity) {
    this.position=this.images.indexOf(image)
    this.updatePool()
  }

  updatePool() {
    const poolStart = Math.max(0, this.position - Math.floor(this.imagePoolLength / 2))
    const poolEnd = Math.min(this.images.length - 1, poolStart + this.imagePoolLength)
    this.imagePool = this.images.slice(poolStart, poolEnd)
  }
  
  prev() { 
    this.position = Math.max(this.position - 1, 0)
    this.updatePool()
  }
  next() { 
    this.position = Math.min(this.position + 1, this.images.length - 1)
    this.updatePool()
  }

}
