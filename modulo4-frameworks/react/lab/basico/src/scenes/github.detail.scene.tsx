import React from "react";
import { Link, useParams } from "react-router-dom";

export const GithubDetailScene: React.FC = () => {
  const { id,  } = useParams();

  return (
    <>
      <h2>Hello from Github Detail page</h2>
      <h3>User Id: {id}</h3>
      <Link to="/github">Back to list page</Link>
    </>
  );
};
