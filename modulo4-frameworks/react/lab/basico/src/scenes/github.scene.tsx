import { Table, TableBody, TableCell, TableHead, TableRow, Typography } from "@material-ui/core";
import React, { FC, memo, useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { ButtonedInput } from "../lib/buttoned-input";
import { Context } from "../core/context";
import { GithubPagination } from "../pods/github/github.pagination";
import { GithubMember } from "../pods/github/github.member";
import { MemberEntity } from "../pods/github/github.model";


export const GithubScene: FC = memo(() => {
    const { filter, result, currentPage, setFilter, setCurrentPage, url, setUrl } = useContext(Context);
    const [ members, setMembers ] = useState<MemberEntity[]>([]);

    useEffect( () => {
        if (!filter) setFilter('lemoncode')
        if (!!url && url.startsWith('https://api.github.com')) {
            setUrl(url)
        } else {
            setUrl(`https://api.github.com/orgs/${filter}/members`)
        }
    }, [])

    useEffect( () => {
        setUrl(`https://api.github.com/orgs/${filter}/members`)
    }, [filter])

    useEffect( () => {
        if (!!result && !(result?.message ?? null) && !(result?.info ?? null)) {
            setMembers(result)
        }
    }, [result])

    return (
        <>
            <Typography variant="h2">Hello from GitLab page</Typography>
            <ButtonedInput
                label={"Mostrar"}
                initialValue={filter}
                onValidate={setFilter}
            />
            <Table className="table">
                <TableHead>
                    <TableRow>
                        <TableCell>Avatar</TableCell>
                        <TableCell>Id</TableCell>
                        <TableCell>Name</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    <GithubPagination {...{
                        elementsPerPage: [3,4,5],
                        currentPage: currentPage,
                        onPageChanged: setCurrentPage,
                    }}
                    >
                    {members.map((member) => (
                        <GithubMember 
                            key={JSON.stringify(member)}
                            {...member} 
                        />
                    ))}
                    </GithubPagination>
                </TableBody>
            </Table>
            <Link to="/main"><Typography>Come back to main menu</Typography></Link>
        </>
    );
})
