import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField, Typography } from "@material-ui/core";
import React, { FC, memo, useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { Context } from "../core/context";
import { useDebounce } from "../lib/use-debounce";
import { RickandmortyCharacter } from "../pods/rickandmorty/rickandmorty.character";
import { RickandmortyPagination } from "../pods/rickandmorty/rickandmorty.pagination";

export const RickandmortyScene: FC = memo(() => {
    const { filter, result, setFilter, url, setUrl } = useContext(Context);
    const [items, setItems] = useState([]);
    const [next, setNext] = useState("");
    const [prev, setPrev] = useState("");
    const [pages, setPages] = useState(0);

    const debouncedFilter = useDebounce(filter, 1000)
    
    useEffect(() => {
        if (!!url && url.startsWith('https://rickandmortyapi.com')) {
            setUrl(url)
        } else {
            setUrl(`https://rickandmortyapi.com/api/character/?name=${filter}`);
        }
    }, []);

    useEffect(() => {
        if (result && result.info) {
            setPages(result.info.pages);
            setNext(result.info.next);
            setPrev(result.info.prev);
            setItems(result.results);
        }
    }, [result]);


    useEffect(() => {
        setUrl(`https://rickandmortyapi.com/api/character/?name=${debouncedFilter}`);
    }, [debouncedFilter]);



    const handleChangeFilter = event => setFilter(event.target.value);
    const handleMovePrevious = () => prev && setUrl(prev);
    const handleMoveNext = () => next && setUrl(next);

    return (
        <>
            <Typography variant="h2">Hello from Rick And Morty page</Typography>
            <TextField label={"Filtrar"} value={filter} onChange={handleChangeFilter} />
            <TableContainer style={{ height: 500 }}>
                <Table stickyHeader className="table" style={{height: "max-content"}}>
                    <TableHead>
                        <TableRow>
                            <TableCell>Avatar</TableCell>
                            <TableCell>Id</TableCell>
                            <TableCell>Name</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <RickandmortyPagination
                            {...{
                                pages,
                                onMovePrevious: handleMovePrevious,
                                onMoveNext: handleMoveNext,
                            }}
                        >
                            {items.map((item) => (
                                <RickandmortyCharacter key={JSON.stringify(item)} {...item} />
                            ))}
                        </RickandmortyPagination>
                    </TableBody>
                </Table>
            </TableContainer>
            <Link to="/main">
                <Typography>Come back to main menu</Typography>
            </Link>
        </>
    );
});
