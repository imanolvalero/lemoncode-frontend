import React from "react";
import { Link, useParams } from "react-router-dom";

export const RickandmortyDetailScene: React.FC = () => {
  const { id } = useParams();

  return (
    <>
      <h2>Hello from Rick And Morty Detail page</h2>
      <h3>User Id: {id}</h3>
      <Link to="/rickandmorty">Back to list page</Link>
    </>
  );
};
