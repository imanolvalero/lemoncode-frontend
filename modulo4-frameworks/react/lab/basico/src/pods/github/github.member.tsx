import React, { PropsWithChildren } from "react";
import { MemberEntity } from "./github.model";
import { Link, generatePath } from "react-router-dom";
import { TableCell, TableRow, Typography } from "@material-ui/core";

export const GithubMember: React.FC = (props: PropsWithChildren<MemberEntity>) => {
    const { avatar_url, id, login } = props
    return (
        <>
            <TableRow>
                <TableCell>
                    <img src={avatar_url} style={{ width: "5rem" }} />
                </TableCell>
                <TableCell>
                    <Typography>{id}</Typography>
                </TableCell>
                <TableCell>
                    <Link to={generatePath("/github/:id", { id: login })}>
                        {login}
                    </Link>
                </TableCell>
            </TableRow>
        </>
    );
};
