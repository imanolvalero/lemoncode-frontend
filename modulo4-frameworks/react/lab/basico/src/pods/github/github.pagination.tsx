import React, { PropsWithChildren } from "react";
import { Pagination } from '../../lib/pagination';

interface Props {
    elementsPerPage: number[],
    currentPage: number,
    onPageChanged: (page: number) => {}
}

export const GithubPagination: React.FC = React.memo((props: PropsWithChildren<Props>) => {
    const {
        elementsPerPage: elementsPerPageRange,
        currentPage: initialCurrentPage,
        onPageChanged
     } = props
    const children = React.Children.toArray(props.children)

    const [currentPage, setCurrentPage] = React.useState<number>(1);
    const [elementsPerPage, setElementsPerPage] = React.useState<number>(elementsPerPageRange[0])
    const [visibleChildren, setVisibleChildren] = React.useState<Array<any>>([])
    const [numPages, setNumPages] = React.useState<number>(0)

    React.useEffect(() => {
        const start:number = (currentPage - 1) * elementsPerPage
        const end:number = Math.min(start + elementsPerPage, children.length)
        updateNumPages()
        updateVisibleItems(start, end)
    }, [elementsPerPage, currentPage]);

    React.useEffect(() => {
        if (children.length) {
            updateNumPages()
            setCurrentPage(initialCurrentPage || 1)
            updateVisibleItems(0, Math.min(elementsPerPage, children.length))
        }
    }, [children.length]);

    const updateNumPages = () => setNumPages(Math.ceil(children.length / elementsPerPage))

    const updateVisibleItems = (start, end) => 
        setVisibleChildren(children.slice(start, end))

    const handlePageChanged = (page:number) => {
        setCurrentPage(page)
        onPageChanged(page)
    }


    return (
        <Pagination {...{
            elementsPerPage: elementsPerPage,
            elementsPerPageRange: elementsPerPageRange,
            numPages: numPages,
            currentPage: currentPage,
            onElementsPerPageChanged: setElementsPerPage,
            onPageChanged: handlePageChanged,
        }}>
            {visibleChildren}
        </Pagination>
    );
});
