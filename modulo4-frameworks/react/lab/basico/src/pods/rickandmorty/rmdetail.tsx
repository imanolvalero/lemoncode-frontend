import React from "react";
import { Link, useParams } from "react-router-dom";

export const RMDetailPage: React.FC = () => {
  const { id } = useParams();

  return (
    <>
      <h2>Hello from Detail page</h2>
      <h3>User Id: {id}</h3>
      <Link to="/rm">Back to list page</Link>
    </>
  );
};
