import React, { PropsWithChildren, useContext } from "react";
import { Context } from "../../core/context";
import { Pagination } from '../../lib/pagination';

interface Props {
    pages: number,
    onMovePrev?: () => {},
    onMoveNext?: () => {},
}

export const RickandmortyPagination: React.FC = React.memo((props: PropsWithChildren<Props>) => {
    const {
        pages,
        onMovePrev,
        onMoveNext,
     } = props

    const children = React.Children.toArray(props.children)
    const {currentPage, setCurrentPage} = useContext(Context);
    const elementsPerPage = 20

    const handleMovePrev = () => {
        setCurrentPage(currentPage <= 1 ? 1 : currentPage - 1)
        onMovePrev()
    }

    const handleMoveNext = () => {
        setCurrentPage(currentPage >= pages ? pages : currentPage + 1)
        onMoveNext()
    }


    return (
        <Pagination {...{
            elementsPerPage: elementsPerPage,
            elementsPerPageRange: [elementsPerPage],
            numPages: pages,
            currentPage,
            onMovePrev: handleMovePrev,
            onMoveNext: handleMoveNext,
        }}>
            {children}
        </Pagination>
    );
});
