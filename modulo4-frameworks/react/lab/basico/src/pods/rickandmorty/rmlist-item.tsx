import React from "react";
import { MemberEntity } from "../../scenes/rickandmorty.scene";
import { Link, generatePath } from "react-router-dom";

export const RMListItem: React.FC = (member: MemberEntity) => {
    return (
        <>
            <tr>
                <td>
                    <img src={member.avatar_url} style={{ width: "5rem" }} />
                </td>
                <td>
                    <span>{member.id}</span>
                </td>
                <td>
                    <Link to={generatePath("/rmd/:id", { id: member.login })}>
                        {member.login}
                    </Link>
                </td>
            </tr>
        </>
    );
};
