import React from "react";
import { CharacterEntity } from "./rickandmorty.model";
import { Link, generatePath } from "react-router-dom";
import { TableCell, TableRow, Typography } from "@material-ui/core";

export const RickandmortyCharacter: React.FC<CharacterEntity> = (character: CharacterEntity) => {
    const { id, name, image } = character
    return (
        <>
            <TableRow>
                <TableCell>
                    <img src={image} style={{ width: "5rem" }} />
                </TableCell>
                <TableCell>
                    <Typography>{id}</Typography>
                </TableCell>
                <TableCell>
                    <Link to={generatePath("/rickandmorty/:id", { id })}>
                        {name}
                    </Link>
                </TableCell>
            </TableRow>
        </>
    );
};
