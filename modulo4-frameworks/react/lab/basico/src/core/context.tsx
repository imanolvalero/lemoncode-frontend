import React from "react";

const contextValue = {
    filter: "",
    result: null,
    currentPage: 1,
    url: '', 
    setFilter: (value: string) => {
        console.log("contexto sin inicializar");
    },
    setUrl: (value: string) => {
        console.log("contexto sin inicializar");
    },
    setCurrentPage: (value: number) => {
        console.log("contexto sin inicializar");
    },
};

export const Context = React.createContext(contextValue);

export const ContextProvider: React.FC = ({ children }) => {
    const [result, setResult] = React.useState<object[]>(null)
    const [filter, setFilter] = React.useState<string>("");
    const [url, setUrl] = React.useState<string>("");
    const [currentPage, setCurrentPage] = React.useState<number>(1);

    React.useEffect(() => {
        if (url) {
            fetch(url)
                .then((response) => response.json())
                .then((json) => setResult(json));
        }
    }, [url]);
    
    React.useEffect(() => {
        setCurrentPage(1)
    }, [filter]);

    return (
        <Context.Provider value={{ filter, result, currentPage, setFilter, url, setUrl, setCurrentPage }}>{children}</Context.Provider>
    );
};
