import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { LoginPage } from "./core/login";
import { MainPage } from "./core/main";
import { ContextProvider } from "./core/context";
import { GithubScene } from "./scenes/github.scene";
import { GithubDetailScene } from "./scenes/github.detail.scene";
import { RickandmortyScene } from "./scenes/rickandmorty.scene";
import { RickandmortyDetailScene } from "./scenes/rickandmorty.detail.scene";


export const App = () => {
  return (
    <ContextProvider>
      <Router>
        <Routes>
          <Route path="/" element={<LoginPage />} />
          <Route path="/main" element={<MainPage />} />
          <Route path="/github" element={<GithubScene />} />
          <Route path="/github/:id" element={<GithubDetailScene />} />
          <Route path="/rickandmorty" element={<RickandmortyScene />} />
          <Route path="/rickandmorty/:id" element={<RickandmortyDetailScene />} />
        </Routes>
      </Router>
    </ContextProvider>
  );
};
