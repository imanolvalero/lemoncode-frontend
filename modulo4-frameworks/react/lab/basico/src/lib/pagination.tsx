import { IconButton, MenuItem, Select, Typography } from "@material-ui/core";
import ArrowBack from '@material-ui/icons/ArrowBack'
import ArrowForward from '@material-ui/icons/ArrowForward'
import React, { PropsWithChildren } from "react";

interface Props {
    elementsPerPage: number,
    elementsPerPageRange: number[],
    numPages: number,
    currentPage: number,
    onElementsPerPageChanged: (page: number) => {},
    onPageChanged?: (page: number) => {},
    onMovePrev?: () => {},
    onMoveNext?: () => {},
}

export const Pagination: React.FC = React.memo((props: PropsWithChildren<Props>) => {
    const {
        elementsPerPage,
        elementsPerPageRange,
        numPages,
        currentPage,
        onElementsPerPageChanged,
        onPageChanged,
        onMovePrev,
        onMoveNext,        
     } = props
    const children = React.Children.toArray(props.children)

    const handleElementsPerPageChanged = (event) =>
        onElementsPerPageChanged(event.target.value)
    const handleMovePrevious = () => {
        onMovePrev && onMovePrev() 
        onPageChanged && onPageChanged(Math.max(1, currentPage - 1))
    }
    const handleMoveNext = () =>{
        onMoveNext && onMoveNext() 
        onPageChanged && onPageChanged(Math.min(numPages, currentPage + 1))
    }

    return (
        <>
        {children}
        <div style={{display: 'flex', gap: '1em', alignItems: 'center'}}>
            <Typography> Items by page:  </Typography>
            <Select value={elementsPerPage} onChange={handleElementsPerPageChanged}>
                {elementsPerPageRange.map(item => 
                    <MenuItem value={item} key={item}>{item}</MenuItem>
                )}
            </Select>
            <span style={{flex: '1'}}/>
            <Typography> Page {currentPage} / {numPages} </Typography>
            <IconButton onClick={handleMovePrevious}> <ArrowBack/> </IconButton>
            <IconButton onClick={handleMoveNext}>  <ArrowForward/> </IconButton>
        </div>
        </>
    );
});
