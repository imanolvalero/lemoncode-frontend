import { useState, useEffect } from "react";

export const useDebounce = (value, time = 1000) => {
    const [debouncedValue, setDebouncedValue] = useState<string>(value);
    
    useEffect(() => {
        let timer = setTimeout(() => {
            setDebouncedValue(value)
        }, time);
        return () => {
            clearTimeout(timer);
        };
    }, [value]);

    return debouncedValue
};
 