import { IconButton, InputAdornment, OutlinedInput } from "@material-ui/core";
import Search from '@material-ui/icons/Search';
import React, { PropsWithChildren } from "react";

interface Props {
    initialValue: string,
    label: string,
    onValidate: (string) => void;
}

export const ButtonedInput: React.FC  = (props: PropsWithChildren<Props>) => {
    const { initialValue, label, onValidate } = props
    const [value , setValue] = React.useState<string>(initialValue || '');
    const [clicked , setClicked] = React.useState<boolean>(false);

    React.useEffect(() => {
        onValidate(value)
        setClicked(false)
    }, [clicked])

    const handleChange = event => setValue(event.target.value)
    const handleClicked = () => setClicked(true)

    return (
        <>
        {/* <TextField value={value} onChange={handleChange} />
        <button onClick={handleClicked}>{label}</button> */}
        <OutlinedInput
            id="outlined-adornment-password"
            type='text'
            value={value}
            onChange={handleChange}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="search"
                  onClick={handleClicked}
                  edge="end"
                >
                  <Search />
                </IconButton>
              </InputAdornment>
            }
            label="Password"
          />
        </>
    )
}