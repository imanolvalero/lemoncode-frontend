import { MemberVm } from "@/pods/member/models/member.vm";
import { ref, reactive } from "vue";

export const appStore = {
  company: ref("Lemoncode"),
  members: reactive([] as MemberVm[]),
};
