export interface MemberVm {
  login: string;
  id: number;
  avatar_url: string;
}

export const createEmptyMember = (): MemberVm => ({
  login: "",
  id: 0,
  avatar_url:
    "https://cdn3.iconfinder.com/data/icons/online-states/150/Photos-512.png",
});
