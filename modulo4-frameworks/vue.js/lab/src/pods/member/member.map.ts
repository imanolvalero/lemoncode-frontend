import { MemberApi } from "./models/member.api";
import { MemberVm } from "./models/member.vm";

export const MemberMapApiToVm = (item: MemberApi): MemberVm => ({
  id: item.id,
  login: item.login,
  avatar_url: item.avatar_url,
});
