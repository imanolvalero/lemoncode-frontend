import { MemberMapApiToVm } from "./member.map";
import { MemberVm } from "./models/member.vm";

export const getMemberList = async (company: string): Promise<MemberVm[]> =>
  (
    await (await fetch(`https://api.github.com/orgs/${company}/members`)).json()
  ).map(MemberMapApiToVm);

export const getMember = async (login: string): Promise<MemberVm> =>
  MemberMapApiToVm(
    await (await fetch(`https://api.github.com/users/${login}`)).json()
  );
