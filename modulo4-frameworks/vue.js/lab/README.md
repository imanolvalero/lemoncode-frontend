# Master Front End XI - Módulo 4.3 - Frameworks - Vue Laboratorio

## Intro

​ Laboratorio del módulo de Vue, lo hemos divido en dos secciones: ​

* **Básico**: si queréis simplemente practicar el mínimo de esta parte y aprobarla (tenéis opciones para subir nota) y centraros en otro framework o librería.

* **Opcional**: para trabajar con la tecnología y empezar a hincar diente.

**IMPORTANTE**: estas prácticas las tenéis que subir a vuestro repositorio de Github (o si lo preferís Gitlab o Bitbucket público). ​

## Básico
### Enunciado
Usando la API de Github, mostrar un listado de los miembros que pertenecen a una organización. Y al clickar en uno de los miembros, mostrar los detalles de un usuario en otra página: ​

* Leer de la API de Github para obtener los miembros de una organización. Que sea Lemoncode, por defecto, al entrar en la aplicación.
Podéis usar `axios`, `ky`, `fetch` directamente, o si empleáis Nuxt, el módulo de `@nuxt/http`.

* Añadir un input y un botón para buscar otra organización, es decir:
  * Se muestra por defecto el listado de miembros de `lemoncode`.
  * Se muestra un input que por defecto tiene como valor: `lemoncode`.
  * El usuario puede teclear otro nombre de organización, por ejemplo: microsoft y, al pulsar el botón de búsqueda, te muestra los miembros de dicha organización.
* Se puede clickar en uno de esos miembros. Al clickar, te lleva a la página detalle de cada uno, donde se muestra su información de perfil.
* Al volver de la página de detalle, muestra la organización que se había tecleado en el filtro (por ejemplo si el usuario tecleó microsoft, se debe seguir viendo microsoft). Hay que conservar esa información entre navegaciones. ​

