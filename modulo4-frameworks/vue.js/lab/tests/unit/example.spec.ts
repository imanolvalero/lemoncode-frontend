import { shallowMount } from "@vue/test-utils";
import MemberList from "@/components/MemberList.vue";

describe("MemberList.vue", () => {
  it("renders props.msg when passed", () => {
    const msg = "new message";
    const wrapper = shallowMount(MemberList, {
      props: { msg },
    });
    expect(wrapper.text()).toMatch(msg);
  });
});
