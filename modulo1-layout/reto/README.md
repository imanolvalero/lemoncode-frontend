# Modulo 1 - Layout - Reto


## ¡CHALLENGE!

[[Solucion en vivo]](https://imanolvalero.gitlab.io/lemoncode-frontend/modulo1-layout/reto/index.html)


### Introducción
Una conocida casa cinematográfica quiere crear su propia plataforma on-line para servir sus producciones, a la que van a llamar WARNER LIVE. 

Nos han solicitado una prueba de maquetación de un primer diseño, del resultado de la prueba dependerá pasar a la siguiente fase de selección.


__Diseño Desktop (ancho 1280px mínimo)__
![desktop](../assets/mod1-reto-desktop.png)


__Diseño Desktop (ancho 1280px mínimo)__
![desktop hover](../assets/mod1-reto-desktop-hover.png)


__Diseño Reponsive__
![responsive widths](../assets/mod1-reto-widths.png)


__Diseño Mobile/Tablet (hasta 1280px)__
![mobile 1](../assets/mod1-reto-responsive-1.png)

__Diseño Mobile/Tablet (hasta 1280px)__
![mobile 2](../assets/mod1-reto-responsive-2.png)



__Notas__
* Fuente utilizada: [Jost](https://fonts.google.com/specimen/Jost?preview.text=warner+live&preview.text_type=custom)
* Color de fondo de la plataforma: `#141414`
* Se adjunta carpeta de trabajo, [aquí](https://campus.lemoncode.net/api/student/assets/ca3c21135bec72f8d8c5f948c58d6b96:11e927114a76cda015acf5f8c44dfcd0:18543284f375c92fd3146469ad983602b9c2e6a5e45d15c454fdf886bebd9e75f23baa4ce3d2ef5e81943007077611d6eb3977e86a8054abbd458c34888eb856d8d7ef3d867735fb200ae4fdd7c745c045fec9f3f0d95e92c029ba46f8d308477ec8065413ee4a47dbcef1323cf42373ada2f5fae542318eaa01cd57f6181775927b6c62b3cd07fa16bed2bb7c4f08c247805400a7f885cdbda988575d0d5e593c7492cb?download=true), con la base de HTML (puede ser modificado al gusto) y las imágenes, carátulas y logo, que se pueden modificar/añadir.
* Se aporta los requisitos de diseño base, con libertad de creatividad.
