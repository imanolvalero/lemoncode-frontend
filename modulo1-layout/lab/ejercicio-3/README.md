# Modulo 1 - Layout - Ejercicio 3

## Crear la barra de navegación de la imagen usando Flexbox. 
[[Solucion en vivo]](https://imanolvalero.gitlab.io/lemoncode-frontend/modulo1-layout/lab/ejercicio-3/index.html)

El html es abierto completamente, es decir crear las estructuras necesarias que se crean convenientes. 
La barra de navegación responde a distintas resoluciones. Utilizar media queries para conseguir este resultado. 
 
Resultado resoluciones mayores: 
 <img src="../../assets/mod1-lab-ejercicio-3a.png" alt="menu laptop" width="500px"/>
Resultado resoluciones menores: 
  <img src="../../assets/mod1-lab-ejercicio-3b.png" alt="menu movil" width="300px"/>
