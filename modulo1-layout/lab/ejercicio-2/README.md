# Modulo 1 - Layout - Laboratorio - Ejercicio 2

## Crear dos temas distintos y mostrar los resultados en una página.

[[Solucion en vivo]](https://imanolvalero.gitlab.io/lemoncode-frontend/modulo1-layout/lab/ejercicio-2/index.html)

Los cambios en el tema afectan a las siguientes características:
• Color
• Fuente
• Border Radius
• Shadow Box
Para visualizar los cambios debería ser suficiente importar un tema u otro en el fichero de estilos principal.

<div style="display:flex;justify-content: space-between;">
    <div style="display:flex;flex-direction:column">
        Resultado tema A: 
        <img src="../../assets/mod1-lab-ejercicio-2a.png" alt="tema a" width="120px"/> 
    </div>
    <div style="display:flex;flex-direction:column">
        Resultado tema B: 
        <img src="../../assets/mod1-lab-ejercicio-2b.png" alt="tema b" width="120px"/> 
    </div>
</div>
 

