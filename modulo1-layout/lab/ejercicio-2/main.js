(() => {
    document.addEventListener("readystatechange", (event) => {
        if (event.target.readyState != "complete") return;
        document.querySelectorAll('input[name="theme"]').forEach((elem) => {
            console.log(elem.value);
            elem.onchange = (event) => {
                const theme = event.target.value,
                    css = `@import url('css/theme_${theme}.css');`;

                document.getElementById("imports").innerHTML = css;
            };
        });
    });
})();
