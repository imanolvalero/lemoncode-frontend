# Modulo 1 - Layout - Ejercicio 4

###  Crearemos un elemento de tipo card con Grid CSS. 
[[Solucion en vivo]](https://imanolvalero.gitlab.io/lemoncode-frontend/modulo1-layout/lab/ejercicio-4/index.html)

Las alineaciones deberán hacerse con esta característica, pero el html es totalmente abierto. 

<img src="../../assets/mod1-lab-ejercicio-4.png" alt="elemento tipo card" width="400px"/>


