# Modulo 1 - Layout - Laboratorio

### Ejercicio 1: Crear una paleta de colores dinámica
[[Ir a la carpeta]](https://gitlab.com/imanolvalero/lemoncode-frontend/tree/master/modulo1-layout/lab/ejercicio-1)[[Solucion en vivo]](https://imanolvalero.gitlab.io/lemoncode-frontend/modulo1-layout/lab/ejercicio-1/index.html)
<img src="assets/ejercicio-1.png" alt="paleta de colores" width="500px"/>

### Ejercicio 2: Crear dos temas distintos y mostrar los resultados en una página
[[Ir a la carpeta]](https://gitlab.com/imanolvalero/lemoncode-frontend/tree/master/modulo1-layout/lab/ejercicio-1)[[Solucion en vivo]](https://imanolvalero.gitlab.io/lemoncode-frontend/modulo1-layout/lab/ejercicio-1/index.html)
<div style="display:flex;justify-content: space-between;">
    <div style="display:flex;flex-direction:column">
        Resultado tema A: 
        <img src="assets/ejercicio-2a.jpg" alt="tema a" width="120px"/> 
    </div>
    <div style="display:flex;flex-direction:column">
        Resultado tema B: 
        <img src="assets/ejercicio-2b.png" alt="tema b" width="120px"/> 
    </div>
</div>

### Ejercicio 3: Crear la barra de navegación de la imagen usando Flexbox
[[Ir a la carpeta]](https://gitlab.com/imanolvalero/lemoncode-frontend/tree/master/modulo1-layout/lab/ejercicio-3)[[Solucion en vivo]](https://imanolvalero.gitlab.io/lemoncode-frontend/modulo1-layout/lab/ejercicio-3/index.html)
<img src="assets/ejercicio-3a.png" alt="menu laptop" width="400px"/>
<img src="assets/ejercicio-3b.png" alt="menu movil" width="200px"/>

### Ejercicio 4: Crearemos un elemento de tipo card con Grid CSS
[[Ir a la carpeta]](https://gitlab.com/imanolvalero/lemoncode-frontend/tree/master/modulo1-layout/lab/ejercicio-4)[[Solucion en vivo]](https://imanolvalero.gitlab.io/lemoncode-frontend/modulo1-layout/lab/ejercicio-4/index.html)
<img src="assets/ejercicio-4.jpg" alt="elemento tipo card" width="200px"/>