# Modulo 1 - Layout - Extra


## Laboratorio EXTRA

[[Solucion en vivo]](https://imanolvalero.gitlab.io/lemoncode-frontend/modulo1-layout/extra/index.html)


### Introducción
Vamos a crear un Layout para una aplicación, que contendrá:

Header
* Toolbar con un input de texto para búsquedas y nombre del usuario logado.
* Barra con el nombre de la aplicación.

Nav
* Menú con varias opciones de navegación

Main
* Contenido con la información principal de la aplicación.
* Fondo diferente de blanco.

Footer
* Texto de la compañía, al final de la página (aunque el contenido no ocupe todo el alto)


### Diseño Desktop (a partir de 768px de ancho)
<img src="../assets/mod1-extra-desktop.png" width="95%" alt="desktop" style="border:1px solid black"/>


<div style="display:flex;justify-content: space-between;">
    <div style="display:flex;flex-direction:column">
        Scroll top
        <img src="../assets/mod1-extra-desktop-scroll-1.png" alt="desktop scroll top" width="90%" style="border:1px solid black"/> 
    </div>
    <div style="display:flex;flex-direction:column">
        Scroll bottom
        <img src="../assets/mod1-extra-desktop-scroll-1.png" alt="desktop scroll bottom" width="90%" style="border:1px solid black"/>
    </div>
</div>


#### Diseño Mobile/Tablet (hasta partir de 768px de ancho)
<div style="display:flex;justify-content: space-between;">
    <div style="display:flex;flex-direction:column">
        Scroll top
        <img src="../assets/mod1-extra-mobile.png" alt="desktop scroll top" width="90%" style="border:1px solid black"/> 
    </div>
    <div style="display:flex;flex-direction:column">
        Scroll bottom
        <img src="../assets/mod1-extra-mobile-scroll.png" alt="desktop scroll bottom" width="90%" style="border:1px solid black"/>
    </div>
</div>


#### Consideraciones

* No perder en ninguna resolución la barra de navegación al hacer scroll
* En resoluciones pequeñas (hasta 768px de ancho):
  * Contenido a ocultar:
    * Barra superior con input de búsqueda y nombre de usuario
    * Título del menú
  * Cambiar menú a la parte superior, bajo el header.
  * Cambiar la organización del contenido principal para visualizarlo sin problemas


#### Libertad de diseño y creatividad:
* Tema
* Contenido
* Breakpoints
* Colores
* Etc…


#### Modificaciones realizadas
* En resoluciones de escritorio (a partir de 768px de ancho):
  * El scroll se limita al frame _main_ manteniendo siempre visible el input de busqueda.


