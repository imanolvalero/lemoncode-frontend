import React from 'react';
import { useHistory, useParams } from 'react-router-dom';
import * as api from './api';
import { CharacterComponent } from './character.component';
import {
  mapCharacterFromApiToVm,
  mapCharacterFromVmToApi,
} from './character.mappers';
import { Character, createEmptyCharacter } from './character.vm';

export const CharacterContainer: React.FunctionComponent = (props) => {
  const [character, setCharacter] = React.useState<Character>(
    createEmptyCharacter()
  );
  const { id } = useParams();
  const history = useHistory();

  const handleLoadCharacter = async () => {
    const apiCharacter = await api.getCharacter(id);
    const bestSentences = await api.getCharacterBestSentences(id);
    console.log(bestSentences)
    setCharacter({
      ...mapCharacterFromApiToVm(apiCharacter),
      bestSentences,
    });
  };

  React.useEffect(() => {
    if (id) {
      handleLoadCharacter();
    }
  }, []);

  const handleSave = async (character: Character) => {
    const success = await api.setCharacterBestSentences(character.id, character.bestSentences);
    if (success) {
      history.goBack();
    } else {
      alert('Error on save character');
    }
  };

  return <CharacterComponent character={character} onSave={handleSave} />;
};
