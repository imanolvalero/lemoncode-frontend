import { Button } from '@material-ui/core';
import {
  TextFieldComponent
} from 'common/components';
import { Form, Formik } from 'formik';
import React from 'react';
import * as classes from './character.styles';
import { formValidation } from './character.validations';
import { Character } from './character.vm';

interface Props {
  character: Character;
  onSave: (character: Character) => void;
}

export const CharacterComponent: React.FunctionComponent<Props> = (props) => {
  const { character, onSave } = props;
  const stringSentences: string = character.bestSentences.join('\n')

  const handleSaveCharacter = (values) => {
    console.log(values)
    onSave({
      ...character,
      bestSentences: values?.stringSentences?.split('\n') ?? character.bestSentences
    })
  }
  

  return (
    <Formik
      onSubmit={handleSaveCharacter}
      initialValues={character}
      enableReinitialize={true}
      validate={formValidation.validateForm}
    >
      {() => (
        <Form className={classes.root}>
          <TextFieldComponent name="name" label="Name" InputProps={{ readOnly: true }}/>
          <TextFieldComponent name="status" label="Status" InputProps={{ readOnly: true }}/>
          <TextFieldComponent name="species" label="Species" InputProps={{ readOnly: true }}/>
          <TextFieldComponent name="gender" label="Gender" InputProps={{ readOnly: true }}/>
          <TextFieldComponent
            name="stringSentences"
            label="Best quotes"
            multiline={true}
            rows={3}
            rowsMax={5}
            defaultValue={stringSentences}
          />
          <Button type="submit" variant="contained" color="primary">
            Save
          </Button>
        </Form>
      )}
    </Formik>
  );
};
