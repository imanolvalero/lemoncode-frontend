import { Character } from './character.api-model';


export const getCharacter = async (id: number): Promise<Character> => 
  await (await fetch(`https://rickandmortyapi.com/api/character/${id}`)).json()

export const getCharacterBestSentences = async (id: number): Promise<string[]> => 
  (await (await fetch(`api/bestSentences?id=${id}`)).json())[0]?.bestSentences ?? []

  export const setCharacterBestSentences = 
  async (id: number, bestSentences: string[]): Promise<boolean> => {
    const res = await (await fetch(`api/bestSentences/${id}`, {
        method: 'put',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ id, bestSentences })
      }
    )).json()
    if (Object.keys(res).length === 0) createCharacterBestSentences(id, bestSentences)
    return true
  }

const createCharacterBestSentences = 
  async (id: number, bestSentences: string[]): Promise<boolean> => 
  (await fetch('api/bestSentences', {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ id, bestSentences })
    }
  )).json()

