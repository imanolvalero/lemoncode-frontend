import { Box } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar/Avatar';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import IconButton from '@material-ui/core/IconButton/IconButton';
import Typography from '@material-ui/core/Typography';
import EditIcon from '@material-ui/icons/Edit';
import * as React from 'react';
import { CharacterEntityVm } from '../character-collection.vm';

interface Props {
  character: CharacterEntityVm;
  onEdit: (id: number) => void;
}

export const CharacterCard: React.FunctionComponent<Props> = (props) => {
  const { character, onEdit } = props;

  return (
    <Card>
      <CardHeader
        avatar={<Avatar aria-label="Character">
          <Box component="img"
            sx={{ height: 30, width: 30 }}
            // @ts-ignore
            src={ 
              character.status === 'Alive' 
              ? 'https://s3.getstickerpack.com/storage/uploads/sticker-pack/pickle-rick-rick-and-morty/sticker_13.png?9aff3e41c31ab27d820e8d52d28aef43'
              : 'https://cdn.onlinewebfonts.com/svg/img_569685.png'
            } 
          />
        </Avatar>}
        title={character.name}
        subheader={`${character.species} - ${character.gender}`}
      />
      <CardContent>
        <div>
          <CardMedia
            image={character.image}
            title={character.name}
            style={{ height: 0, paddingTop: '56.25%' }}
          />
          {character.bestSentences.length > 0 && 
            <Typography variant="subtitle1" gutterBottom>
              { '"' + character.bestSentences.join('", "') + '"' }
            </Typography>
          }
        </div>
      </CardContent>
      <CardActions>
        <IconButton onClick={() => onEdit(character.id)}>
          <EditIcon />
        </IconButton>
      </CardActions>
    </Card>
  );
};
