import { mapFromApiToVm } from '../character-collection.mapper';
import { CharacterEntityVm } from '../character-collection.vm';
import { CharacterEntityApi } from './character-collection.api-model';


export const getCharacterCollection = async (): Promise<CharacterEntityVm[]> => 
  Promise.all(
    (await (await fetch('https://rickandmortyapi.com/api/character')).json())
      .results.map(async (character:CharacterEntityApi): Promise<CharacterEntityVm> => ({
        ...mapFromApiToVm(character),
        bestSentences: await getCharacterBestSentences(character.id)
      })
    )
  )

export const getCharacterBestSentences = async (id: number): Promise<string[]> => 
  (await (await fetch(`api/bestSentences?id=${id}`)).json())[0]?.bestSentences ?? []



