# LemonCode-FrontEnd

Repositorio donde alojar las entregas de la 11ª edición del Master FrontEnd de LemonCode


## Modulo 1 - Layout

Modulo introductorio de CSS

### Entregas
#### Laboratorio [[Ir a la carpeta]](https://gitlab.com/imanolvalero/lemoncode-frontend/tree/master/modulo1-layout/lab/)
1. Crear una paleta de colores dinámica.[[Ir a la carpeta]](https://gitlab.com/imanolvalero/lemoncode-frontend/tree/master/modulo1-layout/lab/ejercicio-1)[[Solucion en vivo]](https://imanolvalero.gitlab.io/lemoncode-frontend/modulo1-layout/lab/ejercicio-1/index.html)

1. Crear dos temas distintos y mostrar los resultados en una página.[[Ir a la carpeta]](https://gitlab.com/imanolvalero/lemoncode-frontend/tree/master/modulo1-layout/lab/ejercicio-2)[[Solucion en vivo]](https://imanolvalero.gitlab.io/lemoncode-frontend/modulo1-layout/lab/ejercicio-2/index.html)
  
1. Crear la barra de navegación de la imagen usando Flexbox.[[Ir a la carpeta]](https://gitlab.com/imanolvalero/lemoncode-frontend/tree/master/modulo1-layout/lab/ejercicio-3)[[Solucion en vivo]](https://imanolvalero.gitlab.io/lemoncode-frontend/modulo1-layout/lab/ejercicio-3/index.html)
  
1. Crearemos un elemento de tipo card con Grid CSS.[[Ir a la carpeta]](https://gitlab.com/imanolvalero/lemoncode-frontend/tree/master/modulo1-layout/lab/ejercicio-4)[[Solucion en vivo]](https://imanolvalero.gitlab.io/lemoncode-frontend/modulo1-layout/lab/ejercicio-4/index.html)


#### Exta: Layout para una aplicación   [[Ir a la carpeta]](https://gitlab.com/imanolvalero/lemoncode-frontend/tree/master/modulo1-layout/extra)[[Solucion en vivo]](https://imanolvalero.gitlab.io/lemoncode-frontend/modulo1-layout/lab/extra/index.html)

#### Reto: Netflix clone [[Ir a la carpeta]](https://gitlab.com/imanolvalero/lemoncode-frontend/tree/master/modulo1-layout/reto) [[Solucion en vivo]](https://imanolvalero.gitlab.io/lemoncode-frontend/modulo1-layout/lab/reto/index.html)
Una conocida casa cinematográfica quiere crear su propia plataforma on-line para servir sus producciones, a la que van a llamar WARREN LIVE. 
Nos han solicitado una prueba de maquetación de un primer diseño, del resultado de la prueba dependerá pasar a la siguiente fase de selección.

## Modulo 2 - Lenguaje
__Nota:__ Todas las soluciones tienen su test unitario
### Ejercicios Básicos [[Ir a la carpeta]](https://gitlab.com/imanolvalero/lemoncode-frontend/tree/master/modulo2-lenguaje/basico/)
* Biggest Word
* Values
* Califications
* Check Arguments
* Clone Merge
* Deep Equal
* Dices
* Hoisting
* Includes
* Primes
* Reverse Text
* Subsets
* This
* Zip
* ZZCrypt

### Ejercicios ESNext [[Ir a la carpeta]](https://gitlab.com/imanolvalero/lemoncode-frontend/tree/master/modulo2-lenguaje/esnext/)
* Args
* Console
* Fibonacci
* Players Order
* Reminder
* Swap

### Retos [[Ir a la carpeta]](https://gitlab.com/imanolvalero/lemoncode-frontend/tree/master/modulo2-lenguaje/retos/)
* Trazas por consola
* Acceso en profundidad
* Aplanando arrays
* Memoization
* Árbol

### Laboratorio [[Ir a la carpeta]](https://gitlab.com/imanolvalero/lemoncode-frontend/tree/master/modulo2-lenguaje/lab/)
* Array operations
* Concat
* Clone Merge
* Read Books
* Slot Machine


## Modulo 3 - Bundling
[[Ir a la carpeta]](https://gitlab.com/imanolvalero/lemoncode-frontend/-/tree/master/modulo3-bundling)

## Laboratorio
[[Ir a la carpeta]](https://gitlab.com/imanolvalero/lemoncode-frontend/-/tree/master/modulo3-bundling/lab)
Implementar una aplicación de `Typescript` simple, con estilos en `SASS` y empaquetada con `webpack`.
                    
## Opcional
Implementar un hola mundo en `React`, con una build de producción, con variables de entorno para diferentes entornos, y una forma de pesar del bundle.

Se pide:
1. Empaquetar con `webpack`. [[Ir a la carpeta]](https://gitlab.com/imanolvalero/lemoncode-frontend/-/tree/master/modulo3-bundling/opcional/bundling-webpack)
2. Empaquetar con `parcel` (v1). [[Ir a la carpeta]](https://gitlab.com/imanolvalero/lemoncode-frontend/-/tree/master/modulo3-bundling/opcional/bundling-parcel)


## Modulo 4 - Frameworks
## Laboratorio React
[[Ir a la carpeta]](https://gitlab.com/imanolvalero/lemoncode-frontend/-/tree/master/modulo4-frameworks/react/lab)
Usando la API de `Github`, mostrar un listado de los miembros que pertenecen a una organización, y mostrar el detalle del miembro seleccionado.

## Laboratorio Angular
[[Ir a la carpeta]](https://gitlab.com/imanolvalero/lemoncode-frontend/-/tree/master/modulo4-frameworks/angular/lab)
Layout completo de una mini-aplicación.

## Laboratorio Vue
[[Ir a la carpeta]](https://gitlab.com/imanolvalero/lemoncode-frontend/-/tree/master/modulo4-frameworks/vue,js/lab)
Usando la API de `Github`, mostrar un listado de los miembros que pertenecen a una organización, y mostrar el detalle del miembro seleccionado.

## Modulo 5 - Testing
## Laboratorio
[[Ir a la carpeta]](https://gitlab.com/imanolvalero/lemoncode-frontend/-/tree/master/modulo4-testing/lab)
Implemtenar pruebas unitarias a componentes y hooks.
                    
## Modulo 6 - REST API
## Laboratorio
[[Ir a la carpeta]](https://gitlab.com/imanolvalero/lemoncode-frontend/-/tree/master/modulo6-restapi/lab)
Vamos a consumir una API pública para mostrar datos de la serie `Rick & Morty`.

## Modulo 7 - Cloud
## Laboratorio
[[Ir a la carpeta]](https://gitlab.com/imanolvalero/lemoncode-frontend/-/tree/master/modulo7-cloud/lab)
Crear un proceso de ci/cd en GitLab Pages.


